module.exports = function (gulp, plugins, path) {
	return () => {

		function getConn() {
			return plugins.ftp.create({
				host: 'kapsula.artbayard.ru',
				user: 'kapsula.artbayard.ru|ftp_kapsula',
				pass: '1234',
				parallel: 10,
			});
		}

		var conn = getConn();

		return gulp.src(path.public.all)
			.pipe(conn.dest('./'));
	}
}