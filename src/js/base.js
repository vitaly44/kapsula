$(document).ready(function () {
    $(".owl-carousel").owlCarousel({
        loop: true,
        dots: true,
        nav: true,
        margin: 0,
        items: 1,
        navText: ['<img src="/img/slider-arrow-left.svg" />', '<img src="/img/slider-arrow-right.svg" />'],
        navContainer: '.owl-navs .nav',
        dotsContainer: '.owl-navs .dots',
    });
});

$(document).ready(function () {
    var $hamburger = $('.hamburger');
    var _body = $('body');
    $hamburger.on('click', function (e) {
        $(this).toggleClass('is-active');

        $('.mob').toggleClass('show');
        $('nav .col').removeClass('is_submenu');
        $('nav .col .submenu').removeClass('show');

        if (_body.hasClass('is_submenu')) {
            _body.removeClass('is_submenu');
        }
    });
});

$(document).ready(function () {
    $('html, body').on('click', function () {
        var _body = $('body');
        var eventInMenu = $(event.target).parents('header');

        if (!eventInMenu.length) {
            hide_submenu();
            $('header .mob').removeClass('show');
            _body.removeClass('is_submenu');
            $(".hamburger").removeClass("is-active");
        }
    })
});